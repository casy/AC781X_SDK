
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* --------------------------------- Includes ---------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* --------------------------------- Exported Types ---------------------------------*/
/* USER CODE BEGIN Exported_Types */

/* USER CODE END Exported_Types */

/* --------------------------------- Exported Constants ---------------------------------*/
/* USER CODE BEGIN Exported_Constants */

/* USER CODE END Exported_Constants */

/* --------------------------------- Exported Macros ---------------------------------*/
/* USER CODE BEGIN Exported_Macros */

/* USER CODE END Exported_Macros */

/* --------------------------------- Defines ---------------------------------*/

/* USER CODE BEGIN Defines */

/* USER CODE END Defines */

#ifdef __cplusplus
 extern "C" {
#endif
/* --------------------------------- Exported Function Prototypes ---------------------------------*/
/* USER CODE BEGIN Exported_Function_Prototypes */

/* USER CODE END Exported_Function_Prototypes */
void Error_Handler(char *, int);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT AutoChips *****END OF FILE****/
