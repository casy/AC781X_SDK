
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors,  any
 * reproduction,  modification,  use or disclosure of AutoChips Software,  and
 * information contained herein,  in whole or in part,  shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE,  RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES,  EXPRESS OR IMPLIED,  INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY,  FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, 
 * INCORPORATED IN,  OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE,  AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE,  AT AUTOCHIPS'S OPTION,  TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE,  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */

/*************<start>******************/


/*************<include>****************/
#include "uart.h"

#if (UART_SAMPLE_SEL == UART_LIN_SAMPLE)
/*************<macro>******************/
#define UART_DATA_BUF_LEN		(15)//UART数据长度选择
	
#define DATA_DIR_RX            	(0)//接收数据
#define DATA_DIR_TX            	(1)//发送数据
	
#define ENHANCED_CHECKSUM    	(0)//增强型校验
#define CLASSIC_CHECKSUM     	(1)//经典型校验
#define CHECKSUM_CALC_SEL		(CLASSIC_CHECKSUM)
	
#define MASTER_MODE          	(0)//主机模式
#define SLAVE_MODE           	(1)//接收模式
#define UART_LIN_MODE_SEL		(SLAVE_MODE)


/*************<enum>*******************/
typedef enum
{
    LIN_STATE_IDLE,                  ///< Idle state
    LIN_STATE_SEND_BREAK,            ///< Send break state
    LIN_STATE_RECEIVE_BREAK,         ///< Receive break state
    LIN_STATE_SEND_SYNC,             ///< Send sync state
    LIN_STATE_RECEIVE_SYNC,          ///< Receive sync state
    LIN_STATE_SEND_IDENTIFIER,       ///< Send identifier state
    LIN_STATE_RECEIVE_IDENTIFIER,    ///< Receive identifier state
    LIN_STATE_SEND_DATA,             ///< Send data state
    LIN_STATE_RECEIVE_DATA,          ///< Receive data state
    LIN_STATE_SEND_CHECKSUM,         ///< Send checksum state
    LIN_STATE_BUSSLEEP               ///< Bus sleep state
}UART_LIN_State;


/*************<union>******************/


/*************<struct>*****************/
typedef struct 
{
    uint8_t  LIN_ID;     //< LIN message frame ID for this frame
    uint8_t  dataDir;    //< Name of the node that publishes the data
    uint8_t  chkSumType; //< Type of checksum to be used (enhanced or classic)
    uint8_t  dataLen;    //< Number of bytes in the data field
    uint16_t delayTime;  //< Actual slot time in ms for the frame
    uint8_t  *dataBuf;  //< Address of the structure that lists the signals
} FRAME_ID_TBL;

typedef struct 
{
    uint8_t  	  frmTtlNum; //< Number of frame slots in the schedule table
    FRAME_ID_TBL  *schedTab;//< Address of the schedule table
} SCHEDULE_TBL;


/*************<variable>***************/
uint8_t 			g_sendDataRdy = FALSE;
uint8_t 			g_recvDataRdy = FALSE;
uint8_t				g_sendDataBuf[UART_DATA_BUF_LEN];
uint8_t				g_recvDataBuf[UART_DATA_BUF_LEN];

uint8_t 			g_UART_LIN_Mode = SLAVE_MODE;
uint8_t 			g_UART_LIN_State = LIN_STATE_IDLE;
uint8_t				g_UART_LIN_DataDir;
uint8_t 			g_UART_LIN_ID;
uint8_t				g_UART_LIN_SchTabIdx;
uint8_t				g_UART_LIN_FrmTabIdx;
uint8_t 			g_UART_LIN_SendByteCnt;
uint8_t 			g_UART_LIN_SendTtlByte;
uint8_t 			g_UART_LIN_RecvByteCnt;
uint8_t				g_UART_LIN_RecvTtlByte;


///< Slave receive header and response data
FRAME_ID_TBL g_frmTab0[] =
{
    {0x03, 0, 0, 8, 60, &g_recvDataBuf[0]},
    {0x05, 1, 0, 8, 60, &g_sendDataBuf[0]},
    {0x03, 0, 0, 8, 60, &g_recvDataBuf[0]},
    {0x05, 1, 0, 8, 60, &g_sendDataBuf[0]},
};

FRAME_ID_TBL g_frmTab1[] =
{
    {0x01, 0, 0, 8, 60, &g_recvDataBuf[0]},
    {0x02, 1, 0, 8, 60, &g_sendDataBuf[0]},
    {0x03, 0, 0, 8, 60, &g_recvDataBuf[0]},
    {0x04, 1, 0, 8, 60, &g_sendDataBuf[0]},
};

///< Schedule table
SCHEDULE_TBL g_schedTab[] =
{
    {4, &g_frmTab0[0]},
	{4, &g_frmTab1[0]},
};
#define UART_LIN_FRM_TAB_MAX	(2)

/*************<prototype>**************/
static void UART_LIN_Recv(void);
static void UART_LIN_Send(void);
static void UART_LIN_Break(void);
static uint8_t UART_LIN_CalcPID(uint8_t id);
static uint8_t UART_LIN_CheckSum(uint8_t pid, uint8_t *pDataBuf, uint8_t dataLen);

void UART6_IRQHandler(void);
int32_t SPM_IRQCallback(uint32_t wparam, uint32_t lparam);

/**
* @prototype UART_Lin_Sample(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 UART LIN sample for master.
*			 UART LIN使用.
*/
void UART_Lin_Sample(void)
{
	GPIO_InitHardwr();
	UART_InitHardwr();
	
	while(1)
	{
		UART_CheckDataDealPrd();
	}
}

/**
* @prototype UART_CheckDataDealPrd(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Check UART data recv or send should be do.
*			 查询是否需要处理UART数据.
*/
void UART_CheckDataDealPrd(void)
{
	if (g_sendDataRdy)//发送UART数据
	{	
//		UART_SendString(g_sendDataBuf, UART_DATA_BUF_LEN);
	}
		
	if (g_recvDataRdy)//数据接收后,处理
	{
		g_recvDataRdy = FALSE;
		
		//处理数据
	}
}

/**
* @prototype UART_InitHardwr(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Initalize UART module.
*			 初始化UART模块.
*/
void UART_InitHardwr(void)
{
	UART_SettingType uartSetting;
	
	GPIO_SetFunc(SLIN_RX_PIN, GPIO_FUNC_3);
	GPIO_SetFunc(SLIN_TX_PIN, GPIO_FUNC_3);
	
	GPIO_SetFunc(SLIN_SLP_PIN, GPIO_FUNC_0);
	GPIO_SetDir(SLIN_SLP_PIN, GPIO_OUTPUT);
	GPIO_SetPinBits(SLIN_SLP_PIN);
	
	uartSetting.baudrate = UART_BAND_RATE_SEL;//设置波特率
	uartSetting.dataBits = 8;
	uartSetting.stopBits = 1;
	uartSetting.parity	 = 0;
	uartSetting.fifoByte = ENABLE;//使能缓冲区
	uartSetting.dmaEn 	 = DISABLE;
	UART_Init(UART6, &uartSetting);//初始化串口
	
	UART_SetLIN(UART6, (LIN_EN | LIN_LBRKIE | LIN_LBRKDL | LIN_LABAUDEN));
	UART_SetRxIntEn(UART6, ENABLE);//使能接收中断
	NVIC_ClearPendingIRQ(UART6_IRQn);
	NVIC_EnableIRQ(UART6_IRQn);
	
	/*使能UART6唤醒休眠MCU.*/
	SPM_EnableModuleWakeup(SPM_MODULE_UART6, TRUE);
	SPM_SetSPMIRQHandler(SPM_IRQCallback);//由于进入stop模式后,UART被关闭,所以唤醒后需要重新初始化UART.
	SPM_EnableModuleSPMIRQ(SPM_MODULE_UART6, TRUE);
}
	
/**
* @prototype UART_LIN_Recv(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Deal the recv data.
			 处理数据接收.
*/
static void UART_LIN_Recv(void)
{
	FRAME_ID_TBL *pFrmTab;
	uint8_t data,ii,jj,checkSum;
	
	data = UART_ReceiveData(UART6);//读取数据
	
	if (g_UART_LIN_DataDir == DATA_DIR_RX)//接收数据
	{
		if (g_UART_LIN_Mode == SLAVE_MODE)//从机模式
		{
			if (g_UART_LIN_State == LIN_STATE_RECEIVE_IDENTIFIER)//0x55用于自动同步,so首先接收到的是ID
			{
				for (ii = 0; ii < UART_LIN_FRM_TAB_MAX; ii++)
				{
					pFrmTab = g_schedTab[ii].schedTab;
					for (jj = 0; jj < g_schedTab[ii].frmTtlNum; jj++)
					{
						if (data == UART_LIN_CalcPID(pFrmTab[jj].LIN_ID))//PID正确
//						if ((data & 0x3F) == pFrmTab[jj].LIN_ID)//ID正确
						{
							g_UART_LIN_ID = pFrmTab[jj].LIN_ID;
							g_UART_LIN_FrmTabIdx = jj;//记住帧索引
							
							if (pFrmTab[jj].dataDir == DATA_DIR_TX)//是主机读取数据
							{
								g_UART_LIN_DataDir = DATA_DIR_TX;
								g_UART_LIN_State = LIN_STATE_SEND_DATA;
								
								g_UART_LIN_SendTtlByte = pFrmTab[jj].dataLen;
								memcpy(g_sendDataBuf, pFrmTab[jj].dataBuf, g_UART_LIN_SendTtlByte);
								
								if (pFrmTab[jj].chkSumType == CLASSIC_CHECKSUM)
								{
									g_sendDataBuf[g_UART_LIN_SendTtlByte] = UART_LIN_CheckSum(0x00, g_sendDataBuf, g_UART_LIN_SendTtlByte);
								}
								else
								{
									g_sendDataBuf[g_UART_LIN_SendTtlByte] = UART_LIN_CheckSum(data, g_sendDataBuf, g_UART_LIN_SendTtlByte);
								}
								g_UART_LIN_SendTtlByte += 1;//加上校验字节
								
								g_UART_LIN_RecvByteCnt = 0;
								g_UART_LIN_SendByteCnt = 0;
							}
							else//接收数据
							{
								g_UART_LIN_DataDir = DATA_DIR_RX;
								g_UART_LIN_State = LIN_STATE_RECEIVE_DATA;
								
								g_UART_LIN_RecvTtlByte = (pFrmTab[ii].dataLen + 1);
								g_UART_LIN_RecvByteCnt = 0;
							}
							
							break;//跳出循环
						}
					}
					if ((g_UART_LIN_State == LIN_STATE_SEND_DATA) || (g_UART_LIN_State == LIN_STATE_RECEIVE_DATA))//已找到ID,不用再找
					{
						break;
					}
				}
				if (ii == UART_LIN_FRM_TAB_MAX)//无效ID
				{
					g_UART_LIN_State = LIN_STATE_IDLE;
				}
			}//if (g_UART_LIN_State == LIN_STATE_RECEIVE_IDENTIFIER)
			else if (g_UART_LIN_State == LIN_STATE_RECEIVE_DATA)//正式接收数据
			{
				if (g_UART_LIN_RecvByteCnt < g_UART_LIN_RecvTtlByte)
				{
					g_recvDataBuf[g_UART_LIN_RecvByteCnt++] = data;
				}
				
				if (g_UART_LIN_RecvByteCnt == g_UART_LIN_RecvTtlByte)
				{
					pFrmTab = g_schedTab[g_UART_LIN_SchTabIdx].schedTab;
					if (pFrmTab[g_UART_LIN_FrmTabIdx].chkSumType == CLASSIC_CHECKSUM)
					{
						checkSum = UART_LIN_CheckSum(0x00, g_recvDataBuf, g_UART_LIN_RecvByteCnt);
					}
					else
					{
						checkSum = UART_LIN_CheckSum(data, g_recvDataBuf, g_UART_LIN_RecvByteCnt);
					}
					
					if (checkSum == g_recvDataBuf[g_UART_LIN_RecvByteCnt])
					{
						g_recvDataRdy = TRUE;
					}
					
					g_UART_LIN_SchTabIdx = 0;
					g_UART_LIN_State = LIN_STATE_IDLE;
				}
			}//if (g_UART_LIN_State == LIN_STATE_RECEIVE_DATA)//正式接收数据
		}
	}
}

/**
* @prototype UART_LIN_Send(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Deal the send data.
			 处理数据发送.
*/
static void UART_LIN_Send(void)
{
	if (g_UART_LIN_Mode == SLAVE_MODE)
	{
		if (g_UART_LIN_State == LIN_STATE_SEND_DATA)
		{
			if (g_UART_LIN_SendByteCnt < g_UART_LIN_SendTtlByte)
			{
				UART_SendData(UART6, g_sendDataBuf[g_UART_LIN_SendByteCnt++]);
				
				if (g_UART_LIN_SendByteCnt == g_UART_LIN_SendTtlByte)
				{
					UART_SetTxIntEn(UART6, 0);
				}
			}
		}
		else
		{
			UART_SetTxIntEn(UART6, 0);
		}
	}
}

/**
* @prototype UART_LIN_Break(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Deal the break.
			 处理分隔符.
*/
static void UART_LIN_Break(void)
{
	if (g_UART_LIN_Mode == SLAVE_MODE)//从机模式
	{
		g_UART_LIN_RecvByteCnt = 0;
		g_UART_LIN_SendByteCnt = 0;
		
		g_UART_LIN_DataDir = DATA_DIR_RX;
		g_UART_LIN_State = LIN_STATE_RECEIVE_IDENTIFIER;
	}
	else //主机模式
	{
		
	}
}

/**
* @prototype UART_LIN_CalcPID(uint8_t id)
*
* @param[in] id:
* @return	 void
*
* @brief  	 Calc PID by id.
*			 通过ID计算PID.
*/
static uint8_t UART_LIN_CalcPID(uint8_t id)
{
	union 
	{
		uint8_t dataByte;
		struct
		{
			uint8_t d0	:1	;
			uint8_t d1	:1	;
			uint8_t d2	:1	;
			uint8_t d3	:1	;
			uint8_t d4	:1	;
			uint8_t d5	:1	;
			uint8_t P0	:1	;
			uint8_t P1	:1	;
		}dataBit;
	}PID;
	
	PID.dataByte = (id & 0x3F);
	PID.dataBit.P0 = ~(PID.dataBit.d1 ^ PID.dataBit.d3 ^ PID.dataBit.d4 ^ PID.dataBit.d5);
	PID.dataBit.P1 =  (PID.dataBit.d0 ^ PID.dataBit.d1 ^ PID.dataBit.d2 ^ PID.dataBit.d4);
	
	return (PID.dataByte);
}

/**
* @prototype UART_LIN_CheckSum(uint8_t pid, uint8_t *pDataBuf, uint8_t dataLen)
*
* @param[in] pid:
* @param[in] pDataBuf:
* @param[in] dataLen: 
* @return	 void
*
* @brief  	 check sum.
*			 计算数据校验.如果是增强型校验,则pid为实际PID.否则pid参数传递0即可.
*/
static uint8_t UART_LIN_CheckSum(uint8_t pid, uint8_t *pDataBuf, uint8_t dataLen)
{
	uint8_t ii;
	uint16_t checkSum = 0;
	
	for (ii = 0; ii < dataLen; ii++)
	{
		checkSum += pDataBuf[ii];
		if (checkSum >= 0x100)
		{
			checkSum -= 0xFF;
		}
	}
	checkSum = (uint8_t)checkSum;
	
	return (~checkSum);
}

/**
* @prototype UART6_IRQHandler(void)
*
* @param[in] ...
* @return	 ...
*
* @brief  	 UART module interrupt handler.
*			 UART 中断处理函数.
*/
void UART6_IRQHandler(void)
{
	// Receive break field
    if (UART_IsLINBreak(UART6))
    {
		UART_LIN_Break();
        UART_ClearLINBreak(UART6);
    }
    // Receive data
    if (UART_RxIsDataReady(UART6))
    {
        UART_LIN_Recv();
    }
    // Transmit data done
    if (UART_TxIsEmpty(UART6))
    {
		UART_LIN_Send();
    }
}

/**
* @prototype SPM_IRQCallback(uint32_t wparam, uint32_t lparam)
*
* @param[in] ...
* @return	 ...
*
* @brief  	 SPM interrupt handler.
*			 SPM中断处理函数.
*/
int32_t SPM_IRQCallback(uint32_t wparam, uint32_t lparam)
{
	if (wparam == (1 << SPM_MODULE_UART6))//判断是否有UART6唤醒
	{
		//处理
	}
	
	return 0;
}

#endif

/*************<end>********************/
