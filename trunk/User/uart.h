
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */

/*************<start>******************/
#ifndef __UART_H__
#define __UART_H__

#ifdef __cplusplus
extern "C" {
#endif

/*************<include>****************/
#include "ac78xx_uart.h"
#include "ac78xx_timer.h"
#include "gpio.h"
	
/*************<macro>******************/

/**************************************************************************************************
	主要功能:
	上电后LED2,LED3处于熄灭状态；
	UART1以100ms的间隔不断发送数据,发送数据时LED3以50ms的频率闪烁.
	UART1接收到数据时LED2以50ms的频率闪烁,300ms内没有接收到任何数据,停止闪烁.
	
	掌握要点:
	如何设置UART引脚功能;如何设置UART数据格式.
 **************************************************************************************************/
#define UART_BASIC_SAMPLE		(0)

/**************************************************************************************************
	主要功能:
	上电后LED2,LED3处于熄灭状态；
	UART1以100ms的间隔不断发送数据,发送数据时LED3以50ms的频率闪烁.
	UART1接收到数据时LED2以50ms的频率闪烁,300ms内没有接收到任何数据,停止闪烁.
	
	掌握要点:
	如何设置UART引脚功能;如何设置UART数据格式.如何使用DMA传输UART数据.
 **************************************************************************************************/
#define UART_DMA_SAMPLE			(1)

/**************************************************************************************************
	主要功能:
	实现从机的接收与发送功能.
	
	掌握要点:
	如何设置UART引脚功能;如何使用SW_LIN.
 **************************************************************************************************/
#define UART_LIN_SAMPLE			(2)//注意:选择LIN例程时,需要将UART_LIN宏写入到设置中的C/C++.

/**************************************************************************************************
	主要功能:
	上电后LED2,LED3处于熄灭状态.
	UART1以100ms的间隔不断发送数据,发送数据时LED3以50ms的频率闪烁.发送5s后停止,MCU休眠.
	UART1接收到数据后,LED2以50ms的频率闪烁,300ms内没有接收到任何数据,停止闪烁.
	
	掌握要点:
	如何设置UART唤醒MCU.MCU被唤醒后需要做哪些处理.
	主要是判断MCU休眠后,哪些外设被关闭,被唤醒后,则需要重新初始化这部分需要使用的外设.
 **************************************************************************************************/
#define UART_WKUP_SAMPLE		(3)
#define UART_SAMPLE_SEL			(UART_BASIC_SAMPLE)


#define UART1_RX_PIN			(GPIO_PB6)
#define UART1_TX_PIN			(GPIO_PB5)
	
#define SLIN_RX_PIN				(GPIO_PA13)
#define SLIN_TX_PIN				(GPIO_PA14)
#define SLIN_SLP_PIN			(GPIO_PA15)

#if (UART_SAMPLE_SEL == UART_LIN_SAMPLE)
#define UART_BAND_RATE_SEL		(19200)//波特率选择
#else
#define UART_BAND_RATE_SEL		(115200)//波特率选择
#endif

/*************<enum>*******************/


/*************<union>******************/


/*************<struct>*****************/


/*************<extern>*****************/	


/*************<prototype>**************/
void UART_InitHardwr(void);
void TIMER_InitHardwr(void);

void UART_CheckDataDealPrd(void);
void UART_CheckLEDBlinkPrd(void);

#if (UART_SAMPLE_SEL == UART_BASIC_SAMPLE)
void UART_Basic_Sample(void);
void UART_SendString(uint8_t *pDataBuf, uint8_t dataLen);
#elif (UART_SAMPLE_SEL == UART_DMA_SAMPLE)
void UART_Dma_Sample(void);
#elif (UART_SAMPLE_SEL == UART_LIN_SAMPLE)
void UART_Lin_Sample(void);
#elif (UART_SAMPLE_SEL == UART_WKUP_SAMPLE)
void UART_Wkup_Sample(void);
void UART_SendString(uint8_t *pDataBuf, uint8_t dataLen);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __LIN_H__ */

/*************<end>********************/
