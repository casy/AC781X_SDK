
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors,  any
 * reproduction,  modification,  use or disclosure of AutoChips Software,  and
 * information contained herein,  in whole or in part,  shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE,  RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES,  EXPRESS OR IMPLIED,  INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY,  FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, 
 * INCORPORATED IN,  OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE,  AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE,  AT AUTOCHIPS'S OPTION,  TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE,  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */

/*************<start>******************/


/*************<include>****************/
#include "uart.h"

#if (UART_SAMPLE_SEL == UART_BASIC_SAMPLE)
/*************<macro>******************/
#define UART_DATA_LEN_SEL			(8)//UART数据长度选择


/*************<enum>*******************/


/*************<union>******************/


/*************<struct>*****************/


/*************<variable>***************/
uint8_t 			g_blinkLED2Flag = FALSE;
uint8_t				g_blinkLED3Flag = FALSE;
uint16_t			g_blinkLED2TimeCnt = 0;
uint16_t			g_blinkLED3TimeCnt = 0;

uint8_t 			g_recvedDataRdy		= 0;
uint16_t			g_recvStopTimeCnt	= 0;
uint8_t 			g_recvDataTtlLen	= 0;
uint8_t 			g_sendDataRdy		= TRUE;
uint16_t			g_sendDataTimeCnt	= 0;

uint8_t				g_sendDataBuf[UART_DATA_LEN_SEL] = {0x11, 0x22, 0x33, 0x44};
uint8_t				g_recvedDataBuf[UART_DATA_LEN_SEL];

/*************<prototype>**************/
void TIM2_IRQnCallBack(uint8_t lparam);//TIM2的回调函数
int32_t UART_IRQHandler(uint8_t port, uint32_t LSR0, uint32_t LSR1);

/**
* @prototype UART_Basic_Sample(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 UART basic sample.
*			 UART 基本应用,发送与接收数据
*/
void UART_Basic_Sample(void)
{
	GPIO_InitHardwr();
	UART_InitHardwr();
	TIMER_InitHardwr();
	
	while(1)
	{
		UART_CheckDataDealPrd();
		
		UART_CheckLEDBlinkPrd();
	}
}

/**
* @prototype UART_CheckDataDealPrd(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Check UART data recv or send should be do.
*			 查询是否需要处理UART数据.
*/
void UART_CheckDataDealPrd(void)
{
	if ((g_sendDataRdy) && (g_sendDataTimeCnt > 99))//发送UART数据
	{
		g_sendDataTimeCnt = 0;
		g_blinkLED3Flag = TRUE;//闪烁LED3
		
		UART_SendString(g_sendDataBuf, UART_DATA_LEN_SEL);
	}
		
	if (g_recvedDataRdy)//数据接收后,处理
	{
		//复制数据到处理缓冲区,获取长度,ID信息
		g_blinkLED2Flag = TRUE;
		
		g_recvedDataRdy = 0;
		//处理数据
		
		g_recvStopTimeCnt = 0;
	}
	else
	{
		if ((g_blinkLED2Flag) && (g_recvStopTimeCnt > 299))//300ms没有接收到任何数据,熄灭
		{
			g_blinkLED2Flag = FALSE;
			
			LED2_OFF;
		}
	}
}

/**
* @prototype UART_CheckLEDBlinkPrd(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Check the led weather to be blinking.
*			 检查LED是否需要闪烁.
*/
void UART_CheckLEDBlinkPrd(void)
{
	if ((g_blinkLED2Flag) && (g_blinkLED2TimeCnt > 49))//闪烁LED2
	{
		g_blinkLED2TimeCnt = 0;
		
		LED2_TOGGLE;
	}
	
	if ((g_blinkLED3Flag) && (g_blinkLED3TimeCnt > 49))//闪烁LED3
	{
		g_blinkLED3TimeCnt = 0;
		
		LED3_TOGGLE;
	}
}

/**
* @prototype UART_InitHardwr(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Initalize UART module.
*			 初始化UART模块.
*/
void UART_InitHardwr(void)
{
	UART_SettingType uartSetting;
	
	GPIO_SetFunc(UART1_RX_PIN, GPIO_FUNC_1);
	GPIO_SetFunc(UART1_TX_PIN, GPIO_FUNC_1);
	
	uartSetting.baudrate = UART_BAND_RATE_SEL;//设置波特率
	uartSetting.dataBits = 8;
	uartSetting.stopBits = 1;
	uartSetting.parity	 = 0;
	uartSetting.fifoByte = ENABLE;//使能缓冲区
	uartSetting.dmaEn 	 = DISABLE;
	UART_Init(UART1, &uartSetting);//初始化串口
	
	UART_SetEventCallback(UART1, UART_IRQHandler);
	UART_SetRxIntEn(UART1, ENABLE);//使能接收中断
	
	NVIC_ClearPendingIRQ(UART1_IRQn);
	NVIC_EnableIRQ(UART1_IRQn);
}
	
/**
* @prototype UART_SendString(uint8_t *pDataBuf, uint8_t dataLen)
*
* @param[in] void
* @return	 void
*
* @brief  	 Send Data.
*			 初始化TIM2模块.
*/
void UART_SendString(uint8_t *pDataBuf, uint8_t dataLen)
{
	for (uint8_t ii = 0; ii < dataLen; ii++)
	{
		UART_SendData(UART1, pDataBuf[ii]);
		while(!UART1->UARTn_LSR0.TC);
	}
}
	
/**
* @prototype TIMR_InitHardwr(void)
*
* @param[in] void
* @return	 void
*
* @brief  	 Initalize TIM2 module.
*			 初始化TIM2模块.
*/
void TIMER_InitHardwr(void)
{
	TIMER_ConfigType timerConfig = {0};
	
	timerConfig.loadValue = (48000000 / (1000 - 1));//定时器时钟只能是外设总线时钟
	timerConfig.linkMode = 0;//不使用多定时器进行链接
	timerConfig.interruptEn = 1;//使能中断
	timerConfig.timerEn = 1;//使能定时器
	
	TIMER_SetCallback(TIMER2,TIM2_IRQnCallBack);//设置中断回调函数
	TIMER_Init(TIMER2,&timerConfig);
}

/**
* @prototype TIM2_IRQnCallBack(uint8_t lparam)
*
* @param[in] ...
* @return	 ...
*
* @brief  	 TIM2 module interrupt handler.
*			 TIM2中断处理函数.
*/
void TIM2_IRQnCallBack(uint8_t lparam)
{
	if (TIMER_GetIntFlag(TIMER2))
    {
        TIMER_ClrIntFlag(TIMER2);
		
		if ((g_blinkLED2Flag) && (g_blinkLED2TimeCnt < 0xFFFF))
		{
			g_blinkLED2TimeCnt++;
		}
		
		if ((g_blinkLED3Flag) && (g_blinkLED3TimeCnt < 0xFFFF))
		{
			g_blinkLED3TimeCnt++;
		}
		
		if ((g_blinkLED2Flag) && (g_recvStopTimeCnt < 0xFFFF))
		{
			g_recvStopTimeCnt++;
		}
		
		if ((g_sendDataRdy) && (g_sendDataTimeCnt < 0xFFFF))
		{
			g_sendDataTimeCnt++;
		}
	}
}

/**
* @prototype UART_IRQHandler(uint8_t port, uint32_t LSR0, uint32_t LSR1)
*
* @param[in] ...
* @return	 ...
*
* @brief  	 UART module interrupt handler.
*			 UART 中断处理函数.
*/
int32_t UART_IRQHandler(uint8_t port, uint32_t LSR0, uint32_t LSR1)
{
	if ((UART1->UARTn_IER.ERXNE == 1) && (LSR0 & LSR0_DR))
    {
        if ((!g_recvedDataRdy) && (g_recvDataTtlLen < UART_DATA_LEN_SEL))//接收数据
		{
			g_recvedDataBuf[g_recvDataTtlLen++] = UART_ReceiveData(UART1);
			
			if (g_recvDataTtlLen == UART_DATA_LEN_SEL)//数据接收完毕
			{
				g_recvedDataRdy = TRUE;
				g_recvDataTtlLen = 0;
			}
		}
    }

    if ((UART1->UARTn_IER.ETXE == 1) && (LSR0 & LSR0_THRE))//发送数据
    {
       
    }

    return 0;
}

#endif
/*************<end>********************/
