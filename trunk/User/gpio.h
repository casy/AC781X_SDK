
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("AutoChips Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to AutoChips Inc. and/or its licensors. Without
 * the prior written permission of AutoChips inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of AutoChips Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * AutoChips Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AUTOCHIPS SOFTWARE")
 * RECEIVED FROM AUTOCHIPS AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. AUTOCHIPS EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES AUTOCHIPS PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE AUTOCHIPS SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN AUTOCHIPS
 * SOFTWARE. AUTOCHIPS SHALL ALSO NOT BE RESPONSIBLE FOR ANY AUTOCHIPS SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AUTOCHIPS'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE AUTOCHIPS SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT AUTOCHIPS'S OPTION, TO REVISE OR REPLACE THE
 * AUTOCHIPS SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO AUTOCHIPS FOR SUCH AUTOCHIPS SOFTWARE AT ISSUE.
 */

/*************<start>******************/
#ifndef __GPIO_H__
#define __GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/*************<include>****************/
#include "ac78xx_gpio.h"
#include "system_ac78xx.h"
#include "ac78xx_debugout.h"

	
/*************<macro>******************/
#define FALSE						(0U)
#define TRUE						(1U)	

/*可通过此宏选择是通过BITBAND访问GPIO还是通过寄存器访问*/
#define USE_BITBAND_TO_ACCES		(TRUE)
	
#define LED2_PIN					(GPIO_PB9)//定义LED2引脚
#define LED3_PIN					(GPIO_PB10)//定义LED3引脚
	
#if (USE_BITBAND_TO_ACCES)
#define LED2						(GPIOB_OUT(9))//定义LED2引脚别名
#define LED3						(GPIOB_OUT(10))//定义LED3引脚别名
			
#define LED2_ON						do{LED2 = 1;}while(0)
#define LED2_OFF					do{LED2 = 0;}while(0)
#define LED2_TOGGLE					do{LED2 = !LED2;}while(0)
	
#define LED3_ON						do{LED3 = 1;}while(0)
#define LED3_OFF					do{LED3 = 0;}while(0)
#define LED3_TOGGLE					do{LED3 = !LED3;}while(0)

#else

#define LED2_ON						do{GPIO_SetPinBits(LED2_PIN);}while(0)
#define LED2_OFF					do{GPIO_ResetPinBits(LED2_PIN);}while(0)
#define LED2_TOGGLE					do{if(GPIO_GetPinValue(LED2_PIN)){LED2_OFF;}else{LED2_ON;}}while(0)
	
#define LED3_ON						do{GPIO_SetPinBits(LED3_PIN);}while(0)
#define LED3_OFF					do{GPIO_ResetPinBits(LED3_PIN);}while(0)
#define LED3_TOGGLE					do{if(GPIO_GetPinValue(LED3_PIN)){LED3_OFF;}else{LED3_ON;}}while(0)
#endif


#define K3_4_5_PIN					(GPIO_PA8)//定义K3,K4,K5引脚
#define K6_PIN						(GPIO_PA11)//定义K6引脚
#define K7_PIN						(GPIO_PA12)//定义K7引脚

#if (USE_BITBAND_TO_ACCES) 
#define K6_IN						(GPIOA_IN(11))//由于是GPIO_PA11,所以传递的参数是bit11
#define K7_IN						(GPIOA_IN(12))//由于是GPIO_PA12,所以传递的参数是bit12
#else
#define K6_IN						(GPIO_GetPinValue(K6_PIN))
#define K7_IN						(GPIO_GetPinValue(K7_PIN))
#endif

/*************<enum>*******************/
typedef enum
{
	GPIO_INPUT	= 0,
	GPIO_OUTPUT	= 1
}GPIO_Dir_TypeDef;

typedef enum
{
	GPIO_FUNC_0	= 0,
	GPIO_FUNC_1	= 1,
	GPIO_FUNC_2	= 2,
	GPIO_FUNC_3	= 3
}GPIO_Func_TypeDef;

typedef enum
{
	KEY_UNDOWN = 0,
	KEY_DOWN = 1,
}KEY_Action_TypeDef;

/*************<union>******************/


/*************<struct>*****************/


/*************<extern>*****************/	


/*************<prototype>**************/
void GPIO_InitHardwr(void);

#ifdef __cplusplus
}
#endif

#endif /* __GPIO_H__ */

/*************<end>********************/
